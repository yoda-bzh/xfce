# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xfce [ module=apps autotools=true require_dev_tools=true ]
require gtk-icon-cache
require freedesktop-desktop

SUMMARY="GStreamer based media player"

HOMEPAGE="http://docs.xfce.org/apps/${PN}/start"
SLOT="0"
PLATFORMS="~amd64"

MYOPTIONS="
    clutter     [[ description = [ Use the clutter backend for the interface ] ]]
    debug
    gtk-doc
    libnotify   [[ description = [ Build the notification plugin ] ]]
    tag         [[ description = [ Enable tag reading with taglib ] ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/dbus-glib:1[>=0.70]
        dev-libs/glib:2[>=2.32.0]
        media-libs/gstreamer:1.0[>=1.0.0]
        sys-apps/dbus[>=0.60]
        x11-libs/gtk+:3[>=3.22.0] [[ note = [ blacklists 3.22.0 ] ]]
        xfce-base/libxfce4ui[>=4.11.0][gtk3]
        xfce-base/libxfce4util[>=4.10.0]
        xfce-base/xfconf[>=4.10.0]
        clutter? (
            x11-libs/clutter[>=1.16.4]
            x11-libs/clutter-gtk[>=1.4.4]
        )
        gtk-doc? ( dev-doc/gtk-doc[>=1.9] )
        libnotify? ( x11-libs/libnotify[>=0.4.1] )
        tag? ( media-libs/taglib:=[>=1.4] )
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    clutter
    debug
    gtk-doc
    'libnotify notify-plugin'
)

src_prepare() {
    edo sed -e "s|pixmapsdir=\".*|pixmapsdir=\"/usr/share/parole/pixmaps\"|"    \
            -i configure.ac
    xfce_src_prepare
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

